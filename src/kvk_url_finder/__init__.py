import sys

from pkg_resources import get_distribution, DistributionNotFound

if sys.version_info[:2] >= (3, 8):
    # TODO: Import directly (no need for conditional) when `python_requires = >= 3.8`
    pass
else:
    pass

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:
    __version__ = "unknown"

LOGGER_BASE_NAME = __name__
CACHE_DIRECTORY = "cache"
PROCESS_MODES = {"url", "kvk"}
